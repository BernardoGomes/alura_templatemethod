﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_TemplateMethod
{
    public class Item
    {
        public string Nome { set; get; }
        public double Valor { set; get; }

        public Item(string Nome, double Valor)
        {
            this.Nome = Nome;
            this.Valor = Valor;
        }
    }
}

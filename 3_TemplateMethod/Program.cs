﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_TemplateMethod
{
    class Program
    {
        static void Main(string[] args)
        {


       


            Orcamento orcamento = new Orcamento();
            orcamento.listaDeItens.Add(new Item("A1", 100));
            orcamento.listaDeItens.Add(new Item("A23", 50));

            Console.WriteLine(new ICPP().Calcula(orcamento) +"#" + new IKCV().Calcula(orcamento) +"#"+ new ImpostoIHIT().Calcula(orcamento));
            
            orcamento.listaDeItens.Add(new Item("A4", 500));
            orcamento.listaDeItens.Add(new Item("A1", 100));
            orcamento.listaDeItens.Add(new Item("A6", 50));
            Console.WriteLine(new ICPP().Calcula(orcamento) + "#" + new IKCV().Calcula(orcamento) + "#" + new ImpostoIHIT().Calcula(orcamento));
            
            Console.ReadKey();
            

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_TemplateMethod
{
    class IKCV : TemplateDeImpostoCondicional
    {

        protected override bool DeveUsarMaximaTaxacao(Orcamento orcamento)
        {

            return orcamento.Valor > 500 && orcamento.listaDeItens.Count(p => p.Valor > 100) > 0;
        }

        protected override double MaximaTaxacao(Orcamento orcamento)
        {
            return orcamento.Valor * 0.1;
        }

        protected override double MinimaTaxacao(Orcamento orcamento)
        {
            return orcamento.Valor * 0.06;
        }
    }
}

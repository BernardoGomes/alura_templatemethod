﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_TemplateMethod
{
    public class Orcamento
    {
        public double Valor { 
            get { return listaDeItens.Sum(p => p.Valor);}
            private set { Valor = value; }
            }
        public IList<Item> listaDeItens;

        public Orcamento()
        {
            listaDeItens = new List<Item>();
        }
    }
}
